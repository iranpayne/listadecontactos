package com.iranhn2020.listadecontactos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.iranhn2020.listadecontactos.adapter.ContactosAdapter
import com.iranhn2020.listadecontactos.databinding.ActivityMainBinding
import com.iranhn2020.listadecontactos.model.Contactos

class MainActivity : AppCompatActivity() {

    /*Late initialization: En el siguiente código básicamente la inicialización se da posteriormente.
    Habitualmente cuando una variable no es de un tipo que acepte null requiere ser inicializada en
    el constructor; sin embargo, no siempre aplica esto como, por ejemplo, en situaciones donde la
    inicialización de las variables se da a través de la inyección de dependencias o en la función
    setup dentro de nuestras pruebas unitarias. Para manejar este tipo de situaciones, sobretodo
    para evitar las verificaciones de valores nulos podemos marcar la propiedad con el modificador
    lateinit (recordemos que para declarar una variable que acepte valores nulos debemos agregar el
    signo de interrogación al nombre de la clase).
    La variable binding contedra la lista de la instacia ActivityMain.xml o noscunducira esta.*/
    private lateinit var binding: ActivityMainBinding //Ctrl + Clic sobre la activity para ir a ella

    /*Tipos de listas: Las colecciones se pueden clasificar en dos grupos, las mutables que se
    pueden editar e inmutables que son solo de lectura. Las listas mutables que poseentodo lo
    anterior, pero también nos da la posibilidad de ir rellenando la lista a medida que lo
    necesitemos, el único inconveniente es que más ineficiente con la memoria. Vamos a definir
    lista mutable, basia o en cero, con el modulo contactos.kt, este contiene una lista de vaiables*/
    private var contactos: MutableList<Contactos> = mutableListOf<Contactos>()

    //vamos a crear una variable para llamar la clase adaptadora esta es de tipo adapter
    // ContactosAdapter.kt donde hay una lista declarada vacia
    //private lateinit var adaptador: ContactosAdapter(context = this)
    private val adaptador: ContactosAdapter by lazy{
        ContactosAdapter(context = this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //La variable binding proporciona una ruta hacia la instancia ActivityMain.xml que alberga
        //androidx.recyclerview.widget.RecyclerView el cual sera inflado por la lista que sea creado
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Este es un metodo que sirve para setear el adaptador o configrarlo de manera inicial
        // contiene tres instancias de asignacion: (1)asigana a la variable adaptador la clase que esta
        // en ContactoAdapter.kt llamada con el mismo nombre, (2) enlaza la variable creada en esta instancia
        //con el objeto recyclerview.widget.RecyclerView, (3) y esto se asigna a la variable adaptador
        setupAdapter()

        // Se ubica la funcion en esta instancia para que al iniciar la aplicacion ejecute accion definida
        loadData()

    }
    // contiene tres instancias de asignacion: (1)asigana a la variable adaptador la clase que esta
    // en ContactoAdapter.kt llamada con el mismo nombre, (2) enlaza la variable creada en esta instancia
    //con el objeto recyclerview.widget.RecyclerView, (3) y esto se asigna a la variable adaptador
    fun setupAdapter(){
      //  adaptador = ContactosAdapter() //la viable toma el nombre de la clase, se creo un referncia directa con la clase
        binding.rvContactos.adapter = adaptador //Amarre entre el adapter y el RecyclerView, usando metodo adapter, y asignarle la instancia creada previa adaptador
        binding.rvContactos.layoutManager = LinearLayoutManager(this) /*Para definir un tipo del cual sera el LayoutManager, se llama al RecyclerView binding.rvContactos
        y asinarle un metodo que se llama layoutManager y se le asigna un LinearLayoutManager con el contexto this dentro de parentesis, con esto
        hemos indicado que es lineal */
        
    }
//Esta funcion funciona como un repositorio de datos que se agregan a la lista mutable contactos declarada
//en esta instancia en la parte superior
    fun loadData() {

        // crear una lista de contactos
        contactos.add(
            Contactos(
                "Tom Hanks",
                "Actor",
                "thanks@aol.com",
                "https://www.biography.com/.image/t_share/MTE1ODA0OTcxNjUxNTk3ODM3/tom-hanks-9327661-1-402.jpg",
                "https://cdn.desiteg.com/src/portal/assets/img/slider/taecel-activa-chips.png",

            )
        )
        contactos.add(
            Contactos(
                "Michael Jordan",
                "Desportista",
                "mjordan@nba.com",
                "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:good%2Cw_1200/MTY2Njc5NDYzOTQ4NDYxNDA4/michael-jordan.jpg",
                "https://cdn.desiteg.com/src/portal/assets/img/slider/taecel-activa-chips.png",

            )
        )
        contactos.add(
            Contactos(
                "Sindy Crawford",
                "Modelo",
                "scrawford@fassion.com",
                "https://m.media-amazon.com/images/M/MV5BMTk4ODYyMzgwOV5BMl5BanBnXkFtZTcwNzIzNDUwMw@@._V1_UY317_CR1,0,214,317_AL_.jpg",
                "https://e7.pngegg.com/pngimages/142/881/png-clipart-iphone-4-telephone-smartphone-cell-gadget-electronics.png"
            )
        )
        contactos.add(
            Contactos(
                "Adele Laurie Blue Adkins",
                "Cantante",
                "adeleblue@sonyrecord.com",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Adele_2016.jpg/220px-Adele_2016.jpg",
                "https://cdn.desiteg.com/src/portal/assets/img/slider/taecel-activa-chips.png"
            )
        )
        contactos.add(
            Contactos(
                "Tom Hanks",
                "Actor",
                "thanks@aol.com",
                "https://www.biography.com/.image/t_share/MTE1ODA0OTcxNjUxNTk3ODM3/tom-hanks-9327661-1-402.jpg",
                "https://cdn.desiteg.com/src/portal/assets/img/slider/taecel-activa-chips.png"
            )
        )
        contactos.add(
            Contactos(
                "Michael Jordan",
                "Deportista",
                "mjordan@nba.com",
                "https://www.biography.com/.image/ar_1:1%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cq_auto:good%2Cw_1200/MTY2Njc5NDYzOTQ4NDYxNDA4/michael-jordan.jpg",
                "https://cdn.desiteg.com/src/portal/assets/img/slider/taecel-activa-chips.png"
            )
        )
        contactos.add(
            Contactos(
                "Sindy Crawford",
                "Modelo",
                "scrawford@fassion.com",
                "https://m.media-amazon.com/images/M/MV5BMTk4ODYyMzgwOV5BMl5BanBnXkFtZTcwNzIzNDUwMw@@._V1_UY317_CR1,0,214,317_AL_.jpg",
                "https://e7.pngegg.com/pngimages/142/881/png-clipart-iphone-4-telephone-smartphone-cell-gadget-electronics.png"
            )
        )
        contactos.add(
            Contactos(
                "Adele Laurie Blue Adkins",
                "Cantante",
                "adeleblue@sonyrecord.com",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Adele_2016.jpg/220px-Adele_2016.jpg",
                "https://cdn.desiteg.com/src/portal/assets/img/slider/taecel-activa-chips.png"
            )
        )

        /* aqui se enlazan la variable arriba declara adaptdor la cual se enlace a ContactosAdapter.pk
        mas la funcion creada en la instancia ContactosAdpater.kt lacual contiene la lista de llena */
        adaptador.updateList(contactos)
    }
}