package com.iranhn2020.listadecontactos.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.iranhn2020.listadecontactos.DestinoActivity
import com.iranhn2020.listadecontactos.R
import com.iranhn2020.listadecontactos.databinding.ItemContactosBinding
import com.iranhn2020.listadecontactos.model.Contactos
import com.squareup.picasso.Picasso

/*Un objeto Adapter actúa como un puente entre AdapterViewlos datos subyacentes y los de esa vista.
 El adaptador proporciona acceso a los elementos de datos. El Adaptador también es responsable de
 crear un android.view.Viewpara cada elemento del conjunto de datos.
 El Adpater es basicamente una clase*/

/*(1)Despues de palabra clase se encuentra un constructor se pueden crear atributos o parametros,
* la cual va se una lista vacia, declarando una variable(var) que sea de tipo lista mutable que
* almacene en este caso la lista contactos(<Contactos>) e inicializandola vasia = mutableListOf()
* se le extentiende una erencia (:RecyclerView.Adapter) solicita entre parentesis que es la que se
* en la parte de abajo ContactosAdapterViewHolder pero se antepone la clase padre ContactosAdapter*/
class ContactosAdapter(
    var contactos: MutableList<Contactos> = mutableListOf(),
    val context: Context
) : RecyclerView.Adapter<ContactosAdapter.ContactosAdapterViewHolder>() {

    /*(2) a continuacion se crea una clase interna que se llama ViewHolder, el cual necesita saber
    * cual es la data que va pintar y cual es el xml (ItemContactos = item_contactos.xml) donde va
    * pintar la informacion, el ViewHolder va recibir una lista la cual se crea dentro de parentesis
    * o parametro, para que esta vista se comporte como un ViewHolder se le extentiende una
    * erencia (:RecyclerView.ViewHolder) solicita entre parentesis una vista la misma es la que se
    * a creado */
    inner class ContactosAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        /*esta bariable se crea para crear un binding de accesso al lista, esta es otra forma de
        * implementar un binding*/
        private val binding: ItemContactosBinding = ItemContactosBinding.bind(itemView)

        fun bind(contactos: Contactos) { /*esta funcion se crea para que va recibir un contacto
        desde el metodo onBindViewHolder(holder: ContactosAdapterViewHolder, position: Int) creado
        an parte de abajo y relacionado por el parametro holder*/

            /* */
            binding.tvNombre.text = contactos.nombre
            binding.tvOcupacion.text = contactos.ocupacion
            binding.tvCorreo.text = contactos.correo
            binding.tvInicial.text = contactos.inicial

            Picasso.get().load(contactos.contacto).into(binding.imgContacto)
            Picasso.get().load(contactos.almacenamiento).into(binding.imgAlmacenamiento)

            itemView.setOnClickListener {
                //println("${contactos.nombre}") //para mostrar por consola

                /*como pasar informacion de una clase a otra? se hace a travez del constructor
                * Toas para mostrar informacion como globo emergente*/
                Toast.makeText(context, "${contactos.nombre}", Toast.LENGTH_SHORT).show()
                /*Bundle es una clase y el texto que sigue es un constructor */
                val bundle = Bundle().apply {
                    putString("key_nombre", contactos.nombre)
                    putString("key_nombre", contactos.ocupacion)
                    putString("key_nombre", contactos.correo)

                    /*Intent es una funcion y tiene su constructor el pide dos cosas donde estoy y asi
                    * donde quiero ir */
                    val intent = Intent(context, DestinoActivity::class.java).apply {

                    }
                }


            }
        }
    }
/*Metodos para el adaptador, para implementar los mismos en la seccion class ContactosAdapter
* aparece un error le damos clic en focoo de color rojo y seleccionamos la opcion  Implement Member*/

    /*Este presenta la vista inflada, view = item_contactos.xml, la misma se pasa al
    * inner class ContactosAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    * dentro sus parentesis */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactosAdapterViewHolder {
        var view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_contactos, parent, false)
        return ContactosAdapterViewHolder(view) //para enviar a ViewHolder
    }

    /* esta funcion recibira una lista de tipo mutable, esta lista se la vamos asignar arriba y se asigna
    * metodo notifyDataSetChanged() el cual se comunica con el RecyclerView que se refreque en automatico
    *   */
    fun updateList(contactos: MutableList<Contactos>) {
        this.contactos = contactos
        notifyDataSetChanged()
    }

    /*este metodo es un como un ciclo for, */
    override fun onBindViewHolder(holder: ContactosAdapterViewHolder, position: Int) {
        val contactos = contactos[position]
        holder.bind(contactos) // esta es una referencia directa entre este metodo y funcion bind
    }

    /*Solicita cuantos elemntos tiene la lista creada, (var contactos: MutableList<Contactos> = mutableListOf() */
    override fun getItemCount(): Int = contactos.size


}