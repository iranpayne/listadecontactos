package com.iranhn2020.listadecontactos.model
/*esta seccion se llama modelos de datos, se crea una  clase para menejar informacion por lo cual
se incorpora la palabra data, se agrgan en parentesis las variables de tipo val
Una data class es una clase que contiene solamente atributos que quedemos guardar, por ejemplo datos
de un superhéroe. Con esto conseguimos por ejemplo no tener varias variables «nombre1», «nombre2»
para almacenar todos los superhéroes con los que vamos a trabajar.*/
data class Contactos(
    val nombre: String,
    val ocupacion: String,
    val correo: String,
    val contacto:String,
    val almacenamiento:String,
    val inicial: String = nombre.substring(startIndex = 0, endIndex = 1)
) {
}